Forms = {};

function isBlank(val){
    return val == "" && val !== false;
}

var emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
    , urlRegex = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;

var Validations = {
    required: function(val, options, values, fieldName){
        if(!val){
            this.errors(fieldName + ' is required.')
        }
    }
    , number: function(val, options, values, fieldName){
        if(!_.isFinite(val)){
            this.errors(fieldName + ' must be a number.')
        }
    }
    , minLength: function(val, length, values, fieldName){
        if(!_.isString(val) || val.length < length){
            this.errors(fieldName + ' must be at least ' + length + ' characters.')
        }
    }
    , maxLength: function(val, length, values, fieldName){
        if(!_.isString(val) || val.length > length){
            this.errors(fieldName + ' must be ' + length + ' characters or less.')
        }
    }
    , isOneOf: function(val, options, values, fieldName){
        if(!_.contains(options, val)){
            this.errors(fieldName + ' must be one of ' + options.join(', '));
        }
    }
    , usPhoneNumber: function(val, options, values, fieldName){
        val = val.replace(/[^0-9]/g, '');
        if(val.length !== 10){
            this.errors(fieldName + ' must be a valid phone number.');
        }
    }
    , positiveNumber: function(val, options, values, fieldName){
        val = Number(val);
        if(val <= 0 || !_.isFinite(val)){
            this.errors(fieldName + ' must be a positive number.');
        }
    }
    , negativeNumber: function(val, options, values, fieldName){
        val = Number(val);
        if(val >= 0 || !_.isFinite(val)){
            this.errors(fieldName + ' must be a negative number.');
        }
    }
    , email: function(val, options, values, fieldName){
        if(!emailRegex.test(val) || val.indexOf('.', val.indexOf('@')) == -1){
            this.errors(fieldName + ' must be a valid email address.');
        }
    }
    , url: function(val, options, values, fieldName){
        if(!urlRegex.test(val)){
            this.errors(fieldName + ' must be a valid url.');
        }
    }
};

if (Meteor.isClient) {
    var isDOMElement = function(o) {
        if (o instanceof jQuery) return true;
        // Code written by SO users mcmlxxxiii and some at http://stackoverflow.com/a/384380
        return typeof HTMLElement === "object" ? o instanceof HTMLElement
                                               : o && typeof o === "object"
                                                   && o !== null && o.nodeType === 1
                                                   && typeof o.nodeName==="string";
    };

    Forms.data = function(form){
        var data = _.reduce($(form).serializeArray(), function(memo, item){
            if(memo[item.name]){
                // we previously have a value for this name
                if(!_.isArray(memo[item.name])){
                    // if it's not already an array, make an array for this set of values
                    // for the same name
                    memo[item.name] = [memo[item.name]];
                }
                memo[item.name].push(item.value);
            }else{
                // saving a new value
                memo[item.name] = item.value;
            }
            return memo;
        }, {});
        return data;
    };
}

Forms.validate = function(values, rules, errors){
    if (Meteor.isClient) values = isDOMElement(values) ? this.data(values) : values;

	if(errors){
		errors.remove({field: { $in: _.keys(rules) }});
	}else{
		errors = new Meteor.Collection(null);
	}

    _.each(rules, function(fieldRules, fieldName){
        var valueUnderTest = values[fieldName];
        _.each(fieldRules, function(ruleOptions, ruleName){
            if(Validations[ruleName]){
                Validations[ruleName].call({errors: function(msg){
                    errors.insert({ field: fieldName, message: msg, validation: ruleName });
                }}, valueUnderTest, ruleOptions, values, fieldName, rules);
            }else if(_.isFunction(ruleOptions)){
                ruleOptions.call({errors: function(msg){
                    errors.insert({ field: fieldName, message: msg, validation: ruleName });
                }}, valueUnderTest, ruleOptions, values, fieldName, rules);
            }else{
                console.error(ruleName + ' is not a known validator or a custom function for field ' + fieldName + ' with value ' + valueUnderTest);
            }
        });
    });
    return errors;
};

Forms.isValid = function(values, rules){
    return Forms.validate(values, rules).find().count() === 0;
};

Forms.handleSubmit = function(template, formSelector, rules, callback, errorCallback) {

    // console.log('fuck me');

 //    var oldCreated = template.created;

 //    template.created = function(){
 //        console.log('inited');
 //    	var self = this;

 //    	self._errors = new Meteor.Collection(null);
 //    	self._focusLog = {};
 //        self._focusLogDep = new Deps.Dependency;

 //    	self.error = function(fieldName){
 //            return self._errors.find({field: fieldName});
 //        };

 //        self.errorMessage = function(fieldName){
 //            var errors = self._errors.find({field: fieldName}).fetch();
 //            return _.pluck(errors, 'message');
 //        };

 //        self.errorClass = function(fieldName){
 //        	return self._errors.find({field: fieldName}).fetch().length === 0 ? '' : 'has-error';
 //        };

	// 	self.errors = function(){
 //            return self._errors.find({},{ sort: { field: 1 }});
 //        };

 //        self.isValid = function(){
 //            return self._errors.find().count() === 0;
 //        };

 //    	self.hasErrors = function(){
 //    	     return self._errors.find().count() !== 0;	
 //    	};

 //        self.isValidated = function(){
 //        	self._focusLogDep.depend();
 //        	return _.keys(self._focusLog).length == _.keys(rules).length;
 //        };

 //        self.disableSubmit = function(){
 //            self._focusLogDep.depend();
 //            try{
 //                var component = self.templateInstance.__component__ || self.templateInstance.__view__;
 //                if(component.dom.isParented){
 //                    return !Forms.isValid(Forms.data(self.templateInstance.find(formSelector)), rules);
 //                }else{
 //                    return false;
 //                }
 //            }catch(e){
 //                // console.log(e);
 //                return false;
 //            }
 //        };

 //        if(oldCreated){
 //            oldCreated.apply(self);
 //        }
	// };

    var submitHandler = {};
    submitHandler['submit ' + formSelector] = function(e, tmpl){
        e.preventDefault();

        console.log('muffins here');

        var form = $(e.currentTarget)
        , input = Forms.data(form)
        , self = this;
        // , component = tmpl.__component__ || tmpl.__view__
        // , errors = component._errors;

        // Forms.validate(input, rules, errors);

        // if(errors.find().count() === 0){
            callback.apply(self, [input, form, e, tmpl])
        // }else if(errorCallback){
            // errorCallback.apply(self, [errors, input, form, e, tmpl]);
        // }
    };

    template.events(submitHandler);    

    var fastHandler = {};
    fastHandler['blur input, blur textarea, change select, keypress input, keypress textarea, keyup input'] = function(e, tmpl){
        // handle keyup events for backspace/delete keys only
        // using keypress for everything else fixes auto-validation 
        // misfires when tabbing through fields
        if(e.type === "keyup" && e.which !== 8 && e.which !== 46){ return; }

        var value = $(e.currentTarget).val()
            , name = $(e.currentTarget).attr('name')
            , input = {}
            , rule = {}
            , component = tmpl.__component__ || tmpl.__view__
            , focusLog = component._focusLog
            , focusLogDep = component._focusLogDep
            , errors = component._errors;

        if(focusLog[name] === true || e.type === "focusout" || e.type === "blur" || e.type === "change"){
            focusLog[name] = true;
            input[name] = value;
            rule[name] = rules[name];

            if(rules[name]){
                Forms.validate(input, rule, errors);
            }

            focusLogDep.changed();
        }
    };

    // template.events(fastHandler); 
};
