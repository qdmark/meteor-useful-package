Package.describe({
  summary: 'In house package for useful io project'
});

Package.on_use(function (api) {

  /*
   * OUR STANDARD APP PACKAGES
   */

  // built-in
  api.use('less');
  api.use('reactive-dict');
  api.use('random');
  api.use('accounts-base');
  api.use('ui');
  api.use('templating');
  api.imply(['reactive-dict', 'random', 'less', 'accounts-base', 'ui', 'templating']);

  // 3rd party
  api.use('collection-hooks');
  api.use('iron-router');
  api.use('moment-with-langs');
  api.use('font-awesome');

  api.imply([
    'iron-router'
    , 'collection-hooks'
    , 'moment-with-langs'
  ]);

  /*
   * UI Components
   */
  api.use('jquery');
  api.use('underscore');
  api.use('mongo-livedata');
  api.use('ejson');

  //Form
  api.add_files('ui_components/forms/forms.js');
  api.export('Forms');

  //linkTo
  api.add_files('ui_components/link_to/link_to.html', 'client');
  api.add_files('ui_components/link_to/link_to.js', 'client');

  /*
   * BOOTSTRAP PACKAGE
   *
   * add the following line at the top of your app.less file
   * @import "/packages/useful/bootstrap/less/bootstrap.lessimport";
   */
  api.use('less');
  api.add_files('bootstrap/js/affix.js', 'client');
  api.add_files('bootstrap/js/alert.js', 'client');
  api.add_files('bootstrap/js/button.js', 'client');
  api.add_files('bootstrap/js/carousel.js', 'client');
  api.add_files('bootstrap/js/collapse.js', 'client');
  api.add_files('bootstrap/js/dropdown.js', 'client');
  api.add_files('bootstrap/js/modal.js', 'client');
  api.add_files('bootstrap/js/scrollspy.js', 'client');
  api.add_files('bootstrap/js/tab.js', 'client');
  api.add_files('bootstrap/js/tooltip-popover.js', 'client');
  api.add_files('bootstrap/js/transition.js', 'client');

  /*
   * USERS PACKAGE
   */
  api.use('templating');
  api.add_files('users/both/collections/users.js', ['client', 'server']);
  api.add_files('users/client/helpers.js', 'client');
  api.add_files('users/server/collections/users.js', 'server');
  api.add_files('users/server/publications/admin_user.js', 'server');
  api.add_files('users/server/publications/admin_users.js', 'server');
  api.add_files('users/server/publications/current_user_data.js', 'server');
  api.export('Users');
});
