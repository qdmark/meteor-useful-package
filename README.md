Compatible Meteor Version
=========================

The Meteor version in your smart.json should be at least:

```
"meteor": {
	"git": "git://github.com/meteor/meteor.git",
	"tag": "release/0.8.3"
}
```

Included Packages
================

* ReactiveDict
* Random
* AccountsBase
* UI
* Templating
* Less
* Bootstrap 3
* FontAwesome
* Collection-Hooks
* IronRouter
* Moment with language helpers
* Users collection

To Use Bootstrap
================

In your application's main less file, add the following line at the top of the file:

`@import "/packages/useful/bootstrap/less/bootstrap.lessimport";`


Users Sub-Package
=================

Provides an updated Users system.
* Users.isUserInRole(userId, role)
* Users.isAdmin(userId)
* {{isUserInRole userId role}}
* {{isAdmin}}
* Meteor.publish('admin_user', userId)
* Meteor.publish('admin_users')
* Meteor.publish('current_user_data') // includes .profile and .roles

Forms Library
=============

For all of the following functions that take a `rules` ruleset object, the following validators are built in:

`required {true}` validates that a field has a value in the input

`number {true}` validates that an input contains a finite number

`minLength {Number}` validates that an input string is at least X characters

`maxLength {Number}` validates that an input string is at most X characters

`isOneOf {Array}` validates that the input matches one of the values in the given array (simple scalar value matching only)

`usPhoneNumber {true}` validates that the input is a 10 digit number, ignore any characters besides numbers, so it can be formatted with dashes, periods, whatever

`positiveNumber {true}` validates that the input is a positive number (> 0)

`negativeNumber {true}` validates that the input is a negative number (< 0)

`email {true}` validates that the input is a properly formatted email address

`url {true}` validates that the input is a properly formatted url

_Passing Custom Validator Functions_

You can also pass in a function to run a custom validator as one of the values for the keys in your ruleset. This validation function takes
the following signature:

`function(val, options, values, fieldName, rules){ ... }`

**Parameters**

	`val {scalar}` the input value to be validated

	`options {mixed}` any options passed to this validator (not used currently)

	`values {Object}` the entire dictionary of key/value pairs that represent the input currently being validated

	`fieldName {String}` the html input name of the value being validated

	`rules {Object}` the entire ruleset being used to validate this form

Inside this validation function the `this` object refers to a special object with handles that assist
in validation. Currently the only provided method on `this` is `errors` which takes a string describing
the validation error if you want to register one. Any message you pass in will show up in the associated
`errors` collection that is return by various functions in the `Forms` library. The documents in the errors
collection have the form `{ field: fieldName, message: msg, validation: ruleName }`. The `field` and `validation`
properties are set for you when you supply a `message` to the `this.errors()` method.

Example

```
var input = {
	email: $('#email-input').val()
	, password: $('#password-input').val()
};
var validationErrors = Forms.validate(input, {
	email: {
		required: true
		, email: true
	}
	, zipCode: {
		usZipCode: function(val, options, values, fieldName, rules){
			var usZipCodeRegex = \^\d{5}(?:[-\s]\d{4})?$\;
			if( !usZipCodeRegex.test(val) ){
				this.errors(fieldName + ' must be a valid US Zip Code of the form XXXXX-XXXX or XXXXX.');
			}
		}
	}
} 
});
```

## `Forms.data(htmlFormObject)`

Parses out the input data from an html form and returns a dictionary of keys/values. Handles array values properly.

**Parameters**

	`form {DOM Object}` JavaScript DOM Object handle to the form you want to process

**Returns**

	`{Object}` dictionary of key/values representing the names of the form inputs and the user entered data in the form

Example

```
Template.TEMPLATE.events({
	'submit form': function(e, tmpl) {
		e.preventDefault()
	
		var input = Forms.data(e.currentTarget)
		...
	}
});
```

## `Forms.validate(values, rules, errors)`

Checks the given dictionary of values against the provided ruleset. 
Optionally uses the passed in errors collection to record validation errors, 
otherwise creates a new anonymous collection and returns that with that validation errors.

**Parameters**

	`values {Object}` JavaScript DOM Object handle to the form whose input values you want to validate OR dictionary of key/value pairs representing the data to be validated

	`rules {Object}` dictionary of key/value pairs representing the name of the value in `values` to be validated along with the given ruleset to validate against

	`errors {Meteor.Collection}` _optional_ collection instance to store validation errors in, if passed in this same errors instance will be returned 

**Returns**

	`{Meteor.Collection}` collection of validation errors (for reactivity), the documents in this collection look like `{ field: fieldName, message: msg, validation: ruleName }`

Example 1

```
Template.TEMPLATE.events({
	'submit form': function(e, tmpl) {
		e.preventDefault()

		var validationErrors = Forms.validate(e.currentTarget, {
        	email: {
        		required: true
        		, email: true
        	}
        	, password: {
        		required: true
        		, minLength: 8
        	}
	}
});
```

Example 2

```
var input = {
	email: $('#email-input').val()
	, password: $('#password-input').val()
};
var validationErrors = Forms.validate(input, {
	email: {
		required: true
		, email: true
	}
	, password: {
		required: true
		, minLength: 8
	}
});
```

## `Forms.isValid(values, rules)`

Checks the given dictionary of values against the provided ruleset. Returns true if all validations pass, false otherwise.

**Parameters**

	`values {Object}` JavaScript DOM Object handle to the form whose input values you want to validate OR dictionary of key/value pairs representing the data to be validated

	`rules {Object}` dictionary of key/value pairs representing the name of the value in `values` to be validated along with the given ruleset to validate against

**Returns**

	`{Boolean}` true if all `values` passed validation for the given `rules`

Example 1

```
Template.TEMPLATE.events({
	'submit form': function(e, tmpl) {
		e.preventDefault()

		var isValid = Forms.isValid(e.currentTarget, {
        	email: {
        		required: true
        		, email: true
        	}
        	, password: {
        		required: true
        		, minLength: 8
        	}
	}
});
```

Example 2

```
var input = {
	email: $('#email-input').val()
	, password: $('#password-input').val()
};
var isValid = Forms.isValid(input, {
	email: {
		required: true
		, email: true
	}
	, password: {
		required: true
		, minLength: 8
	}
});
```

## `Forms.handleSubmit(template, formSelector, rules, callback, errorCallback)`

Wraps a template's submit event for the given `formSelector`, validates the input using 
the passed in `rules` and calls `callback` with the validated input when validation passes
and `errorCallback` when validation fails.

**Parameters**

	`template {Template Definition Object}` the Meteor Template definition object (class) that you want to wrap form validation of, e.g. `Template.SignIn`

	`formSelector {String}` jQuery selector for the form in this template you want to handle the submission of, e.g. `form` or `#credit-card-form`

	`rules {Object}` dictionary of key/value pairs representing the name of the value in `values` to be validated along with the given ruleset to validate against

	`callback {Function}` function to call when validation passes on form submit. Inside the function, `this` refers to the template instance containing the form that was submitted.

		The callback function receives four values

			* `input {Object}` dictionary of key/value pairs representing the data that was submitted

			* `form {jQuery DOM Object}` the form that was submitted, wrapped in jQuery

			* `e {jQuery Event}` the original jQuery wrapped submit event that triggered the validation (same as in a normal template event handler)

			* `tmpl {Meteor Template instance}` the original Meteor template instance that contains the submitted form (same as in a normal template event handler)

	`errorCallback {Function}` function to call when validation _fails_ on form submit. Inside the function, `this` refers to the template instance containing the form that was submitted.

		The callback function receives five values

			* `errors {Meteor.Collection}` collection of validation errors (for reactivity), the documents in this collection look like `{ field: fieldName, message: msg, validation: ruleName }`

			* `input {Object}` dictionary of key/value pairs representing the data that was submitted

			* `form {jQuery DOM Object}` the form that was submitted, wrapped in jQuery

			* `e {jQuery Event}` the original jQuery wrapped submit event that triggered the validation (same as in a normal template event handler)

			* `tmpl {Meteor Template instance}` the original Meteor template instance that contains the submitted form (same as in a normal template event handler)



The behavior of the validation is:

	* validate on submit

	* validated on blur

	* for selects validate on value change

	* for text inputs that have lost focus at least once, do realtime validation on each keypress

It also adds a number of convenience helpers onto the given template to aid in displaying
validation errors.

**Added Handlebars Template Helpers**

	`{{error "fieldName"}}` returns a Cursor that contains all the validation error objects for the given field.

	`{{errorMessage "fieldName"}}` returns an array of just the validation error messages for the given field.

	`{{errorClass "fieldName"}}` returns 'has-error' if the given field doesn't pass validation

	`{{errors}}` returns a Cursor with all the errors from the last validation of the form

	`{{isValid}}` returns true if the form has no validation errors, false otherwise

	`{{isValidated}}` returns true if the form has been validated once, false otherwise

	`{{disableSubmit}}` returns true if the entire form input is not validated and valid, use it like <button disabled={{disableSubmit}}></button


Example

```
Forms.handleSubmit(Template.Register, 'form', {
    name: {
      required: true
      , minLength: 3
    }
    , email: {
      required: true
      , email: true
    }
    , accountType: {
      required: true
      , isOneOf: ['banana','pajamas']
    }
  }, function(input, form, e, tmpl){
    console.log(arguments);
    // success! Submit the form
  }, function(errors, form, e, tmpl){
  	// validation error, log it out for debugging
  	errors = errors.find({field: fieldName}).fetch();
	console.log( _.pluck(errors, 'message') );
  });
```
